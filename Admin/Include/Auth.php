<?php

class Auth{
	var $UserID;
	var $UserName;
	var $UserRol;
	var $UserRolName;

	function Auth($CUserID, $CUserName,$CUserRol,$CUserRolName){
		$this->UserID = $CUserID;
		$this->UserName = $CUserName;
		$this->UserRol = $CUserRol;
		$this->UserRolName = $CUserRolName;
	}
	function getId(){
		return $this->UserID;
	}
	function getName(){
		return $this->UserName;
	}
	function getRolId(){
		return $this->UserRol;
	}
	function getRolName(){
		return $this->UserRolName;
	}
	function Permiso($conexion, $pantalla){
		$sql = "";
		$result = $conexion->query($sql);
		if($result->num_rows>0){
			return true;
		}else{
			return false;
		}
	}
	function ValidaSesion(){
		if(isset($_SESSION['UsuarioID'])){
			return true;
		}else{
			$this->LogOut();
		}
	}
	function LogOut(){
		session_destroy();
		die("<script>location.href = '../index.php'</script>");
	}

}


?>