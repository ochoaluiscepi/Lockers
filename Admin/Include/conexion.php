﻿<?php
class Conexion{
	var $Host;
	var $User;
	var $Pass;
	var $bd;
	var $debug;
	function Conexion(){
		$this->Host = "10.97.39.40";
		$this->User = "lockersuser";
		$this->Pass = "LockersUser12345";
		$this->bd = "lockers";
		$this->debug = false;
	}
	function ConexionBD(){
		try{
			$mysqli = new mysqli($this->Host, $this->User, $this->Pass, $this->bd,3306,"/var/lib/mysql/mysql.sock");
	
			if ($mysqli->connect_errno) {
    			return "Error de conexión!!! ". $mysqli->connect_error." No.". $mysqli->connect_errno;
				}
			$mysqli->set_charset("utf8");
			return $mysqli;
		 }catch(Exception $e){
                     $bd->ErrorLog($bd->ConexionBD(), 'Conexion-ConexionBD',$e);
                     return $e;
         }
		
	}
	function ExecuteQueryResult($Conexion, $Query){
		 try{
					$result = $Conexion->query($Query);
                    if($result->num_rows>0){
                            $row = $result->fetch_array(MYSQLI_ASSOC);
                            return $row;      
                      }else{
                            return null;    
                      }
		 }catch(Exception $e){
                $bd->ErrorLog($bd->ConexionBD(), 'Conexion-ExecuteQueryResult()',$e);
         }
                
	}
	function ExecuteQueryResults($Conexion, $Query){
		 try{
		 	
					$result = $Conexion->query($Query);
                    if($result->num_rows>0){
                            return $result;      
                      }else{
                            return null;    
                      }
		 }catch(Exception $e){
                $bd->ErrorLog($bd->ConexionBD(), 'Conexion-ExecuteQueryResult()',$e);
         }
                
	}
	function ExecuteInsertUpdateDelete($Conexion, $Query){
		try{
					if($Conexion->query($Query)===true){   
                            return true;      
                        }else{
                            return false;    
                        }
		 }catch(Exception $e){
                    $bd->ErrorLog($bd->ConexionBD(), 'Conexion-ExecuteInsertUpdateDelete()',$e);
         }
				
	}
	function ErrorLog($Conexion, $Pantalla, $Error){
		$Query = "INSERT INTO ErrorLog (Pantalla, Error, Fecha) Values('".$Pantalla."','".$Error."',CURRENT_TIMESTAMP)"; 
		if($Conexion->query($Query)===true){   
                            return true;      
                        }else{
                            return false;    
                        }
	}
	function Debug(){
		if($this->debug){
			error_reporting(E_ALL);
			ini_set('display_errors', '1');
		}else{
			error_reporting(~E_ALL);
			ini_set('display_errors', '0');
		}
	}
}	
?>

<?php ?>