<?php session_start();
include 'Include/Auth.php';
include 'Include/conexion.php';
$attr = "index";
$raiz = 1;
$Sesion = new Auth($_SESSION['UsuarioID'],$_SESSION['UsuarioNombre'],$_SESSION['UsuarioRolID'],$_SESSION['UsuarioRol']);
$bd = new Conexion();
$Sesion->ValidaSesion();
$mesActual = intval(date("m"));
//var_dump($mesActual);
$mesAnterior = intval(date("m")) == 1 ? 12:(intval(date("m"))-1);
//var_dump($mesAnterior);

 ?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Dashboard</title>

     <?php include "Include/lib/bootstrapBasic1.php";  ?>

    <!-- Morris -->
    <link href="css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

     <?php include "Include/lib/bootstrapBasic2.php";  ?>

</head>

<body class="fixed-navigation">
    <div id="wrapper">
    <?php include "Include/menu.php";
    $TEXTO1 = "Guadalajara";
    $TEXTO2 = "Mapa";
    $TEXTO3 = "Lockers"; ?>

        <div id="page-wrapper" class="gray-bg">
        <?php include "Include/bannerSuperior.php"; ?>
      <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                   
                    <div class="ibox-content">
               


                
<div class="tabs-container">
                            <ul class="nav nav-tabs">
                     
                           
                   <li class="active"><a data-toggle="tab" href="#tab-1" > Campus Guadalajara</a></li>
                   <li ><a data-toggle="tab" href="#tab-2" > Prepa Tec Santa Anita</a></li>
                                   
                                
                            </ul>
                            <div class="tab-content">
                                
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="row">

                                <div class="col-lg-12">
                                        <div class="ibox">
                                       
                                        <div class="ibox-content text-center">
                                        <img src="img/Campus.jpg" style="width:60%">
                                        </div>
                                        </div>
                                </div>
                    
                                </div>
                                   
                                     </div>
                                </div> 
                                <div id="tab-2" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">

                                <div class="col-lg-12">
                                        <div class="ibox ">
                                       
                                        <div class="ibox-content text-center">
                                        <img src="img/Campus.jpg" style="width:10%">
                                        </div>
                                        </div>
                                </div>
                    
                                </div>
                                   
                                     </div>
                                </div> 
  
                        </div>
</div>
           

            </div> </div> </div> </div> </div>
        <?php include "Include/footer.php"; ?>

        </div>
    </div>

    <!-- Mainly scripts -->
       <?php include "Include/lib/bootstrapBasic3.php";  ?>

    <!-- Flot -->
    <?php include "Include/lib/bootstrapBasic5flot.php";  ?>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>
    <script src="js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Jvectormap -->
    <script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="js/plugins/chartJs/Chart.min.js"></script>
<?php $mes = intval(date("m"));
    $Chart = "";
    $PDisponibles = "";
    $PRegistrados = "";
    //var_dump($mes);
    $ban = 0;
   
     
?>
<input type="hidden" id="PDisponibles" value="<?php echo  $PDisponibles;?>">
<input type="hidden" id="PRegistrados" value="<?php echo  $PRegistrados;?>">
    <script>

        $(document).ready(function() {
            var Mes = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
            var d = new Date();
            var n = d.getMonth();
            var Disponibles = [];
            var Dispo = $("#PDisponibles").val().split(",");
            var Reg = $("#PRegistrados").val().split(",");
           
            for(x=0;x<=n;x++)
            {
                Disponibles.push(Mes[x]);

            }
            var lineData = {
                labels: Disponibles,
                datasets: [
                    {
                        label: "Puntos Registrados",
                        fillColor: "rgba(220,220,220,1)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: Dispo
                    },
                    {
                        label: "Puntos Disponibles",
                        fillColor: "rgba(26,179,148,0.5)",
                        strokeColor: "rgba(26,179,148,0.7)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: Reg
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            };


            var ctx = document.getElementById("lineChart").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

        });
    </script>
</body>
</html>
