<?php session_start();
include 'Include/Auth.php';
include 'Include/Conexion.php';
$bd = new Conexion();
$attr = "clientes";
$raiz = 1;
$Sesion = new Auth($_SESSION['UsuarioID'],$_SESSION['UsuarioNombre'],$_SESSION['UsuarioRolID'],$_SESSION['UsuarioRol']);

$Sesion->ValidaSesion(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Clientes</title>
    <?php include "Include/lib/bootstrapBasic1.php";  ?>
    <!-- Data Tables -->
    <link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
    <?php include "Include/lib/bootstrapBasic2.php";  ?>

</head>

<body>

    <div id="wrapper">
        <?php include "Include/menu.php";
        $TEXTO1 = "Usuarios";
        $TEXTO2 = "Guadalajara";
        $TEXTO3 = "Reservación de lockers"; 
if($_SESSION['UsuarioRolID']==2 || $_SESSION['UsuarioRolID']=='2'){
$Sesion->LogOut();
}

         ?>
   

        <div id="page-wrapper" class="gray-bg">
            <?php include "Include/bannerSuperior.php"; ?><br>
        <form action="" method="POST">
                        <div class="row">
                            <div class="col-md-3">
                                <select name="Edificio" id="Edificio" required class="form-control">
                                    <option value="0">Todos</option>
                                    <?php
                                    $edif = "SELECT * FROM caulas";
                                    $au = $bd->ExecuteQueryResults($bd->ConexionBD(),$edif);
                                    while ($rows = $au->fetch_array(MYSQLI_ASSOC)) {
                                        echo "<option value='".$rows["id"]."'>".$rows["nombre"]."</option>";
                                    }

                                    ?>
                                </select>
                                <input type="hidden" name="EdificioN" id="EdificioN" value="Todos">
                            </div>
                            <div class="col-md-2" id="PisoC" style="display:none">
                                <select name="Piso" id="Piso" class="form-control" >
                                    <option value="0">Todos</option>
                                </select>
                                <input type="hidden" name="PisoN" id="PisoN" value="Todos">
                            </div>
                            <div class="col-md-2" >
                                <select name="Estatus" id="Estatus" class="form-control" >
                                    <option value="0">Todos</option>
                                    <option value="1">Disponible</option>
                                    <option value="2">Solicitado</option>
                                    <option value="3">Asignado</option>
                                </select>
                                <input type="hidden" name="EstatusN" id="EstatusN" value="Todos">
                            </div>
                                
                            <div class="col-md-3">
                                <input type="submit" value="Buscar" class="btn btn-success"> 
                            </div>
                        </div>
                        </form>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <?php 
                        $ban=0;
                        $Edificio = 0;
                        $Piso =0;
                        $Estatus =0;
                        if($_POST){
                            $ban=1;
                            $Edificio = $_POST['Edificio'];
                            $Piso = $_POST['Piso'];
                            $Estatus = $_POST['Estatus'];
                            echo "Edificio: <b>".$_POST['EdificioN']."</b>";
                            echo "   Piso: <b>".$_POST['PisoN']."</b>";
                            echo "   Estatus: <b>".$_POST['EstatusN']."</b>";
                        }

                        ?>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                           
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <?php 
                    $SQL = "SELECT IF(EXISTS(SELECT c.* FROM candado c WHERE c.id=cl.candado),(SELECT CONCAT(c.id,' - ',c.clave)FROM candado c WHERE c.id=cl.candado),NULL)AS candado, cl.folio,cl.matricula,cl.estatus,cl.id_aulas,cl.id_piso,cl.nf,cl.identificador,p.nombre AS piso,ca.nombre AS aula,e.color,e.nombre as nestatus FROM clockers cl inner join caulas ca on ca.id=cl.id_aulas inner join piso p on p.id=cl.id_piso inner join estatus e on e.id=cl.estatus WHERE 1=1 ";
                    $SQL = $Edificio != 0 ? $SQL." AND ca.id='". $Edificio."' ":$SQL;
                    $SQL = $Piso != 0 ? $SQL." AND (p.id_aulas='".$Edificio."' AND  p.numero_piso='". $Piso."') ":$SQL;
                    $SQL = $Estatus != 0 ? $SQL." AND e.id='". $Estatus."'":$SQL;
                    $res = $bd->ExecuteQueryResults($bd->ConexionBD(),$SQL);

                    ?>
                    <div class="ibox-content">
                    <table class="table dataTables-example">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Candado</th>
                                <th>Matrícula</th>
                                <th>Estatus</th>
                                <th>Aulas</th>
                                <th>Piso</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(count($res))
                               while ($row = $res->fetch_array(MYSQLI_ASSOC)) {
                                echo "<tr>";
                                echo "<td>".$row['folio']." - ".$row['identificador']."</td>";
                                echo "<td>".$row['candado']."</td>";
                                echo "<td>".$row['matricula']."</td>";
                                echo "<td   id='locker-".$row['folio']."' style='".$row['color']."'><a data-toggle='modal' data-target='#myModal2'  onclick='Confirmar(\"".$row['folio']."\",\"".$_SESSION['UsuarioID']."\",\"".$row['matricula']."\")'>".$row['nestatus']."</a></td>";
                                echo "<td>".$row['aula']."</td>";
                                echo "<td>".$row['piso']."</td>";
                                echo "</tr>";
                               }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Candado</th>
                                <th>Matrícula</th>
                                <th>Estatus</th>
                                <th>Aulas</th>
                                <th>Piso</th>
                            </tr>
                        </tfoot>
                    </table>
                    

                    </div>
                </div>
            </div>
            </div>
            
        </div>
        <?php include "Include/footer.php"; ?>

        </div>
        </div>



    <!-- Mainly scripts -->
       <?php include "Include/lib/bootstrapBasic3.php";  ?>

    <!-- Data Tables -->
       <?php include "Include/lib/bootstrapBasic4DataTable.php";  ?>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {
            $('.dataTables-example').dataTable({
                responsive: true,
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                            },
                    "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}
            });

            /* Init DataTables */
            var oTable = $('#editable').dataTable();

            /* Apply the jEditable handlers to the table */
            oTable.$('td').editable( '../example_ajax.php', {
                "callback": function( sValue, y ) {
                    var aPos = oTable.fnGetPosition( this );
                    oTable.fnUpdate( sValue, aPos[0], aPos[1] );
                },
                "submitdata": function ( value, settings ) {
                    return {
                        "row_id": this.parentNode.getAttribute('id'),
                        "column": oTable.fnGetPosition( this )[2]
                    };
                },

                "width": "90%",
                "height": "100%"
            } );
            $("#Edificio").change(function(){
                var sel = $("#Edificio option:selected").val();
                var text = $("#Edificio option:selected").text();
                $("#EdificioN").val(text);
                if(sel!=0){
                 $.ajax({
                url:   'Include/AjaxDatosPiso.php?edificio='+sel,
                cache:false,
                type:  'get',
                beforeSend: function () {
                        
                },
                success:  function (response) {
                        $("#Piso").html(response);
                        $("#PisoC").css({"display":"block"});
                },
                error: function (response){
                   
                }
        });
}else{
     $("#Piso").html("<option value='0'>Todos</option>");
    $("#PisoC").css({"display":"none"});
}



            });
            $("#Piso").change(function(){
                var sel = $("#Piso option:selected").val();
                var text = $("#Piso option:selected").text();
                $("#PisoN").val(text);
             });
            $("#Estatus").change(function(){
                var sel = $("#Estatus option:selected").val();
                var text = $("#Estatus option:selected").text();
                $("#EstatusN").val(text);
             });

        });

        function fnClickAddRow() {
            $('#editable').dataTable().fnAddData( [
                "Custom row",
                "New row",
                "New row",
                "New row",
                "New row" ] );

        }
        function LoadModal(folio){
            $.ajax({
                url:   'Include/AjaxDatosCliente.php?folio='+folio,
                cache:false,
                type:  'get',
                beforeSend: function () {
                        $("#ContenidoModal").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#ContenidoModal").html(response);
                },
                error: function (response){
                    $("#ContenidoModal").html(response);
                }
        });
        }
function Confirmar(folio,usr, matricula){
           

//alert(folio+" "+ usr + matricula);



                    $.ajax({
                url:   'Include/AjaxDatosConfirmar.php?folio='+folio+'&usr='+usr,
                type:  'get',
                beforeSend: function () {
                        $("#ContenidoModal").html("<p>Procesando, espere por favor...</p>");
                },
                success:  function (response) {
                    if(response==1){
                        //$("#myModal2").modal('hide');
                      //  $("#locker-"+folio).css({"background-color":"#f3e793"});
                      //  $("#txt-"+folio).html(folio+"<br>Cancelar");
                    var Admin = '<div class="modal-header"><h4 class="modal-title">'+usr+'</h4>';
                      Admin+='<small class="font-bold">Al oprimir el botón, autorizo se me haga el cargo correspondiente por el uso del locker </small> </div><div class="modal-body text-justify" id="ContenidoPorAjax9">';
                      Admin+='<div class="row text-center">';
                      Admin+='<form class="m-t" role="form" action="#" method="POST"><input type="hidden" name="matricula" value="'+usr+'" /><input type="hidden" name="id_locker" value="'+folio+'" /><div class="form-group"> <input type="text" class="form-control" placeholder="Nombre" required="" name="nombre"></div><div class="form-group"><input type="text" class="form-control" placeholder="Carrera" required="" name="carrera"></div> <div class="form-group"> <input type="email" class="form-control" placeholder="email" required="" name="correo"></div><input type="submit" class="btn btn-primary block full-width m-b" value="Aceptar"/></form>';
                      Admin+='<div id="msg"></div></div> </div> <div class="modal-footer">';
                      Admin+='</div>';
                    $("#md").html(Admin);
                    
                    }else if(response==2){
                        $("#myModal2").modal('hide');
                        $("#locker-"+folio).css({"background-color":""});
                        $("#locker-"+folio).html(folio+"Disponible");
                        $("#MiLocker").css({'display':'none'});
                    }else if(response==3){
                         $("#md").html("Lo sentimos tu solicitud no fue procesada");
                    }
                    else if(response==4){
                        $("#ContenidoModal").html("<p>Ya has solicitado un locker</p>");
                    }
                    else if(response==5){
                        $("#ContenidoModal").html("<p>Tu locker se encuentra en estatus de asignado</p>");
                    }else if(response==6){
                        usr = matricula==usr ? usr:matricula;
                      var Admin = '<div class="modal-header"><h4 class="modal-title">Administrador</h4>';
                      Admin+='<small class="font-bold">Asigna, libera o aprueba un solicitande de locker </small> </div><div class="modal-body text-justify" id="ContenidoPorAjax9">';
                      Admin+='<div class="row text-center"> <label for="IDL">Usuario:</label><input type="text" id="IDL" value="'+usr+'" class="form-control"> <div id="msg"></div></div> </div> <div class="modal-footer">';
                      Admin+='<button type="submit" class="btn btn-primary" onclick="Asignar(1,\''+folio+'\',\''+usr+'\')"><i class="fa fa-floppy-o" aria-hidden="true"></i>  Asignar</button>';
                      Admin+='<button type="button" class="btn btn-danger" onclick="Asignar(0,\''+folio+'\',\''+usr+'\')"><i class="fa fa-times" aria-hidden="true"></i> No autorizar</button>';
                      Admin+='<button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-exclamation" aria-hidden="true"></i> Cancelar</button></div>';

                    $("#md").html(Admin);


                    }else{
                        $("#md").html("Error!!! Comunicate con el administrador");
                    }
                    
                },
                error: function (response){
                    $("#md").html(response);
                } });
            
        }
        function Asignar(estatus, folio, usr=''){
            usr = usr=='' ? $("#IDL").val():usr;
            if(usr!='' || estatus==0){
             $.ajax({
                url:   'Include/AjaxDatosClienteAdmin.php?folio='+folio+'&usr='+usr+'&estatus='+estatus,
                type:  'get',
                beforeSend: function () {
                        $("#ContenidoModal").html("<p>Procesando, espere por favor...</p>");
                },
                success:  function (response) {
                    if(response==1 || response=='1'){
                        $("#myModal2").modal('hide');
                        $("#locker-"+folio).css({"background-color":"#f19d9d"});
                        $("#locker-"+folio).html(folio+"Asignado");
                    }else if(response==0 || response=='0'){
                        $("#myModal2").modal('hide');
                        $("#locker-"+folio).css({"background-color":""});
                        $("#locker-"+folio).html("Disponible");
                    }else if(response==2 || response=='2'){
                        $("#myModal2").modal('hide');
                        $("#locker-"+folio).css({"background-color":""});
                        $("#locker-"+folio).html("Disponible");
                    
                    }else {
                        $("#md").html("Error!!! Comunicate con el administrador");
                    }
                    
                },
                error: function (response){
                    $("#msg").html(response);
                }
        });
    }else{
        $("#msg").html("<div class='alert alert-danger text-center'><b>Error!!! Debes ingresar la matrícula</b></div>");
    }
        }
    </script>
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" >
                                    <div class="modal-content animated flipInY" id="md">
                                        <br><div class="row text-center" id="ContenidoModal"><img src="img/loading.gif"></div>
                                        <br>
                                    </div>
                                </div>
                            </div>
</body>

</html>
