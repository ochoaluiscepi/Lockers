<?php  session_start();header('Access-Control-Allow-Origin: *');header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Login</title>

    <link href="Admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="Admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="Admin/css/animate.css" rel="stylesheet">
    <link href="Admin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">LK</h1>

            </div>
            <h3>Bienvenido</h3>
            <p>Sistema de reservación de lockers.
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p>GDL</p>
            <form class="m-t" action="https://sidb5.gda.itesm.mx:7789/escolar/estacionamiento.autentif" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control" id="Username" name="user" placeholder="A00000000" required="required">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="password" name="psswd" placeholder="Password" required="required">
                </div>
                 <input type="hidden" class="form-control" id="link" name="link" value="http://sisinfo.gda.itesm.mx:8000/lockers/">
                <input type="button" class="btn btn-primary block full-width m-b" value="Accesar" id="Acceso"/>

                <!--
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>-->
            </form>
            <!--<p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>-->
            <?php
            if(isset($_SESSION['UsuarioID'])){
                        die("<script>location.href = 'Admin/index.php'</script>");
                     
                }
                if($_POST && !isset($_SESSION['UsuarioID']))
                    if(isset($_POST['user']) && $_POST['aut']==1){
                    try{
                    include 'Admin/Include/conexion.php';
                    $bd = new Conexion(); 
                    $bd->Debug();
                  
                        $user = $_POST['user'];
                        $sql = "SELECT cu.nomina, cu.nom, cr.descripcion, cr.folio as RolId FROM cuser cu,crol cr WHERE cu.rol = cr.folio AND cu.Nomina='$user'";
                        $result = $bd->ExecuteQueryResult($bd->ConexionBD(), $sql);
                        if($result != null){
                                $_SESSION['UsuarioID'] = $result['nomina'];
                                $_SESSION['UsuarioNombre'] = $result['nom'];
                                $_SESSION['UsuarioRol'] = $result['descripcion'];
                                $_SESSION['UsuarioRolID'] = $result['RolId'];
                                die("<script>location.href = 'Admin/index.php'</script>");
                               
                        }else{
                               $_SESSION['UsuarioID'] = $_POST['user'];
                                $_SESSION['UsuarioNombre'] = $_POST['user'];
                                $_SESSION['UsuarioRol'] = "Usuario";
                                $_SESSION['UsuarioRolID'] = 2;
                                die("<script>location.href = 'Admin/index.php'</script>");
                        }
                   
                }catch(Exception $e){
                    $bd->ErrorLog($bd->ConexionBD(), 'index-Home',$e);
                }
                }else if(isset($_SESSION['UsuarioID'])){
                     echo "<div class='alert alert-danger text-center'> <b> Usuario o contraseña incorrecta </b> </div>"; 
                     
                }

             ?>
             <div class='alert alert-danger text-center' id="Result" style="display:none"> <b> Usuario o contraseña incorrecta </b> </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="Admin/js/jquery-2.1.1.js"></script>
    <script src="Admin/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#Acceso").click(function(){
            var user= $("#Username").val();
            var psw = $("#password").val();
            var datos = {
                        "user":user,
                        "psswd":psw,
                        "link":"http://sisinfo.gda.itesm.mx:8000/lockers/indexAjax.php"
                    };
                if(user!=undefined && psw!=undefined && user!="" && psw!=""){
                    $("#Result").css({"display":"block"});
                    $("#Result").html("<b> bien echo</b>");

                  
/*
                     $.ajax({
                url:'https://sidb5.gda.itesm.mx:7789/escolar/estacionamiento.autentif',
                type:'post',
                dataType: 'jsonp',
                data:datos,
                beforeSend: function () {
                   
                },
                success:  function (response) {
                   alert(response);
                    
                },
                error: function (response){
                   
                }
        });*/
                     $.ajax({
                data:  datos,
                //url:   'recive.php',
                url:   'https://sidb5.gda.itesm.mx:7789/escolar/estacionamiento.autentif',
                type:  'post',
                crossOrigin: true,
                contentType: "application/x-www-form-urlencoded",
                headers: {  'Access-Control-Allow-Origin': '*' },
                beforeSend: function () {
                     //   $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        alert(response);
                }
        });

                }else{
                    $("#Result").css({"display":"block"});
                    $("#Result").html("<b> Los campos no pueden estar vacios</b>");
                }
            

        });

    });


    </script>
</body>

</html>
