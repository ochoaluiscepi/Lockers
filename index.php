<?php  session_start();?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Login</title>

    <link href="Admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="Admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="Admin/css/animate.css" rel="stylesheet">
    <link href="Admin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">LK</h1>

            </div>
            <h3>Bienvenido</h3>
            <p>Sistema de reservación de lockers.
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p>GDL</p>
            <form class="m-t" action="https://sidb5.gda.itesm.mx:7789/escolar/estacionamiento.autentif" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control" id="Username" name="user" placeholder="A00000000" required="required">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="Password" name="psswd" placeholder="Password" required="required">
                </div>
                 <input type="hidden" class="form-control" id="link" name="link" value="http://10.97.27.83/Lockers/">
                 <input type="hidden" class="form-control" id="p_falso" name="p_falso" value="true">
                <button type="submit" class="btn btn-primary block full-width m-b">Accesar</button>

                <!--
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>-->
            </form>
            <!--<p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>-->
            <?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
$mysqli = new mysqli("10.97.39.40", "lockersuser", "LockersUser12345", "lockers", 3306);
if ($mysqli->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

echo $mysqli->host_info . "\n";



            if(isset($_SESSION['UsuarioID'])){
                        die("<script>location.href = 'Admin/index.php'</script>");
                     
                }
                 if($_POST && !isset($_SESSION['UsuarioID']))
                    if(isset($_POST['user']) && $_POST['aut']==1){
                    try{
                    include 'Admin/Include/conexion.php';
                    $bd = new Conexion(); 
                    $bd->Debug();
                  
                        $user = $_POST['user'];
                        $sql = "SELECT cu.nomina, cu.nom, cr.descripcion, cr.folio as RolId FROM cuser cu,crol cr WHERE cu.rol = cr.folio AND cu.Nomina='$user'";
                        $result = $bd->ExecuteQueryResult($bd->ConexionBD(), $sql);
                        if($result != null){
                                $_SESSION['UsuarioID'] = $result['nomina'];
                                $_SESSION['UsuarioNombre'] = $result['nom'];
                                $_SESSION['UsuarioRol'] = $result['descripcion'];
                                $_SESSION['UsuarioRolID'] = $result['RolId'];
                                die("<script>location.href = 'Admin/index.php'</script>");
                               
                        }else{
                               $_SESSION['UsuarioID'] = $_POST['user'];
                                $_SESSION['UsuarioNombre'] = $_POST['user'];
                                $_SESSION['UsuarioRol'] = "Usuario";
                                $_SESSION['UsuarioRolID'] = 2;
                                die("<script>location.href = 'Admin/index.php'</script>");
                        }
                   
                }catch(Exception $e){
                     $bd->ErrorLog($bd->ConexionBD(), 'index-Home',$e);
                     var_dump($e);
                }
                }else{
                     echo "<div class='alert alert-danger text-center'> <b> Usuario o contraseña incorrecta </b> </div>"; 
                     
                }

             ?>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="Admin/js/jquery-2.1.1.js"></script>
    <script src="Admin/js/bootstrap.min.js"></script>
</body>

</html>
